# find-mp3-duplicates

This is a script to help you find duplicates in your music library. It supports id3 v2 tagging (not the old v1), on .mp3 files.

It simply tells you which files look like duplicates, based on song and artist names, after some normalization.

## System requirements ##

Currently our script is tested only on Linux (Ubuntu 19 to be specific). Contributions to expand support are welcome.

Dependencies:
* mid3v2 (Mutagen)
* PHP

## Usage ##

Just execute through PHP, giving your music library path:
```
php find-mp3-duplicates.php ~/Music
```

If you don't provide a path, it will assume ~/Music.
