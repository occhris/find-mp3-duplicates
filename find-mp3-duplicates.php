<?php

/**
 * find-mp3-duplicates
 *
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License 3.0
 */

if (strpos(shell_exec('mid3v2'), 'Usage:') === false) {
    throw new Exception('mid3v2 must be installed');
}

set_time_limit(0);
ini_set('memory_limit', '-1');

$folder = isset($_SERVER['argv'][1]) ? $_SERVER['argv'][1] : ($_SERVER['HOME'] . '/Music');

$files = array();
$it = new RecursiveDirectoryIterator($folder);
foreach(new RecursiveIteratorIterator($it) as $filepath) {
    if (substr(substr($filepath, -4)) == '.mp3') {
        $files[] = $filepath;
    }
}

$cnt = count($files);
$songs = array();
$of = 0;
foreach ($files as $filepath) {
    $of++;

    echo "Reading {$filepath} ({$of}/{$cnt})\n";

    $result = shell_exec('mid3v2 ' . escapeshellarg($filepath));

    $matches = array();

    if (preg_match('#TIT2=(.*)#', $result, $matches) != 0) {
        $song = normalize($matches[1]);
    } else {
        $song = null;
    }

    if (preg_match('#TPE1=(.*)#', $result, $matches) != 0) {
        $artist = normalize($matches[1]);
    } else {
        $artist = null;
    }

    if (($song !== null) && ($artist !== null)) {
        $song_label = $artist . ': ' . $song;
        if (!isset($songs[$song_label])) {
            $songs[$song_label] = array();
        }

        $songs[$song_label][] = $filepath;
    }
}

echo "\n";

foreach ($songs as $song_label => $files) {
    if (count($files) > 1) {
        echo "Found likely duplication...\n";
        foreach ($files as $file) {
            echo "{$file}\n";
        }
        echo "\n";
    }
}

echo "Done!\n";

function normalize($song)
{
    return str_replace(array('\'', '&', ' feat. ', ' feat ', ', '), array('', 'and', ' ft. ', ' ft. ', ' and '), strtolower($song));
}
